(function($) {
    $('.stores').dropdown();

    $('#scroll-top-button').on('click', function(ev) {
        ev.preventDefault();

        var body = $("html, body");

        body.stop().animate({ scrollTop: 0 }, 500, 'swing', function() {});
    })

    var games = $('#games'); // Main container.
    var page = 2; // The first page is already loaded, start from the 2nd.
    var loading = false;
    var end = false;
    var colPerRow = 4;
    var heightOffsetTrigger = 1000;
    var loader = $('.loader');

    var store = $('input[name="store"]').val(),
        genre = $('input[name="genre"]').val(),
        rating = $('input[name="rating"]').val(),
        s = $('#s').val();

    $(window).on('scroll', function(ev) {
        var scroll = window.scrollY;
        var height = games.height();

        // Arbitrary height trigger.
        if (scroll > (height - heightOffsetTrigger)) {
            if (!loading && !end) {
                loading = true;
                loader.fadeIn(500);
                var url = `/?page=${page}&store=${store}`;

                if (genre) {
                    url += `&genre=${genre}`;
                }

                if (rating) {
                    url += `&rating=${rating}`;
                }

                if (s) {
                    url += `&s=${s}`;
                }

                $.post(url, function(data) {
                    data = JSON.parse(data);

                    if (data.games.length == 0) {
                        end = true; // No more results, prevent further loading.
                    } else {
                        for (var i = 0; i < data.games.length; i++) {
                            // Batches of 4 items per row.
                            if (i % colPerRow == 0) {
                                var row = $('<div>').addClass('row');
                                games.append(row);
                            }

                            img = data.games[i].image || 'img/no-preview.png';

                            // Differentiate Steam from other stores.
                            if (store == 'Steam') {
                                var gameContent = `
                                    <a href="${data.games[i].steamUrl}" target="_blank">
                                        <img src="${img}" >
                                        <p>${data.games[i].title}</p>
                                    </a>
                                `;
                            } else {
                                var gameContent = `
                                    <img src="${img}" >
                                    <p>${data.games[i].title}</p>
                                `;
                            }

                            var col = $(`
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <div class="game">
                                        ${gameContent}
                                    </div>
                                </div>
                            `)

                            row.append(col);
                        }
                    }

                    loader.fadeOut(500);
                    loading = false;
                    page++;
                });
            }
        }
    });
}(jQuery));
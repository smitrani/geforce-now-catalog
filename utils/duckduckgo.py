from typing import Dict
import requests
import re
import json
import time

def search(keywords: str) -> Dict:
    throttle = 5
    url = 'https://duckduckgo.com/'
    request_url = url + "i.js"
    params = { 'q': keywords }

    # Obtain the token
    while True:
        res = requests.post(url, data=params)
        vqd = re.search(r'vqd=([\d-]+)\&', res.text, re.M|re.I)

        if vqd:
            break
        else:
            time.sleep(throttle)

    headers = {
        'authority': 'duckduckgo.com',
        'accept': 'application/json, text/javascript, */* q=0.01',
        'sec-fetch-dest': 'empty',
        'x-requested-with': 'XMLHttpRequest',
        'user-agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'referer': 'https://duckduckgo.com/',
        'accept-language': 'en-US,enq=0.9',
    }

    params = (
        ('l', 'us-en'),
        ('o', 'json'),
        ('q', keywords),
        ('vqd', vqd.group(1)),
        ('f', ',,,,,'),
        ('p', '1'),
        ('v7exp', 'a'),
    )

    while True:
        try:
            res = requests.get(request_url, headers=headers, params=params)
            data = json.loads(res.text)
            return data
        except:
            time.sleep(throttle)

import requests, json

def rating(app_id):
    url = f'https://store.steampowered.com/appreviews/{app_id}?json=1'
    response = requests.get(url)
    data = json.loads(response.text)

    return data

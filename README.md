# GeForce NOW Catalog

This application makes it easier to find what's in the GFN catalog, the online gaming service (https://www.nvidia.com/it-it/geforce-now/games/)

I have always found the existing catalog very limited so i decided to build my own, I hope you find it useful and you like it.

The project is currently hosted in Heroku

https://gfn-catalog.herokuapp.com/

I'm not a designer so pardon me for the rough look :) If you want to make any change, just fork the project i'll be more than happy to merge your changes!

This application is not related in any ways to nvidia, I don't get any money from this project, I've just built it because I needed it and I think such a service was just missing.

## Set-up instruction

The project is based on Python and [Flask](https://flask.palletsprojects.com/en/2.0.x/). The best way to run it is to

1. create a virtual environment in the `venv` directory, which is ignored already. See [here](https://docs.python.org/3/tutorial/venv.html) how to do it.
2. install all the required dependencies using the `requirements.txt` file by running `pip install -r requirements.txt` from the root directory.
3. set-up a local [mongo database](https://hub.docker.com/_/mongo) using Docker. The official mongo image is OK, just pick the latest version.
4. set-up a local `.env` file that contains the `FLASK_DEBUG=1` variable, useful to run the project locally and the `MONGO_URI` variable containing the URI of your database. See the `.env.dist` file for reference
5. import the game catalog from nVidia running the `parse.py` script
6. run the project locally running `flask run` from the root directory
7. Done!

Please feel free to open an issue and open pull requests!
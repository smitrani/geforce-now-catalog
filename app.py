import os
from flask import Flask
from dotenv import load_dotenv

load_dotenv()

def create_app():
    app = Flask(__name__)

    from catalog import bp as catalog_bp
    from static import bp as static_bp

    app.register_blueprint(catalog_bp)
    app.register_blueprint(static_bp)

    return app

from flask import (
    Blueprint, send_from_directory
)

bp = Blueprint('static', __name__)

@bp.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory('fonts', path)

@bp.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)

@bp.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

@bp.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('img', path)

from crypt import methods
from bson.json_util import dumps
import pymongo, os, math

from flask import (
    Blueprint, render_template, request, jsonify
)

from utils.steam import rating

bp = Blueprint('catalog', __name__)

client = pymongo.MongoClient(os.getenv('MONGO_URI'))
db = client.games
collection = db.games

@bp.route("/", methods=['GET', 'POST'])
def list():
    limit = 100
    stores = collection.distinct('store')
    genres = collection.distinct('genres')
    ratings = collection.distinct('rating')
    store = request.args.get('store')
    genre = request.args.get('genre')
    current_page = request.args.get('page')
    current_rating = request.args.get('rating')
    s = request.args.get('s')
    query = {}

    if current_page is None:
        current_page = 1
    else:
        current_page = int(current_page)

    if store is None:
        store = "Steam"
        query['store'] = 'Steam'
    elif store == '':
        store = ''
        query['store'] = ''
    elif store == '<all>':
        pass
    else:
        query['store'] = store

    if genre is not None:
        query['genres'] = {'$in': [genre]}

    if current_rating is not None:
        query['rating'] = {'$eq': current_rating}

    if s:
        query['title'] = {'$regex': s.strip(), '$options': 'i'}

    total = collection.count_documents(query)
    pages = int(math.ceil(float(total) / float(limit)))
    games = collection.find(query).limit(limit).skip((current_page - 1) * limit)

    if request.method == 'POST':
        return dumps({
            'total': total,
            'current_page': current_page,
            'current_genre': genre,
            'current_store': store,
            'pages': pages,
            's': s,
            'stores': stores,
            'games': games,
        })
    else:
        return render_template('list.html', s=s, games=games, stores=stores, current_store=store,
            total=total, pages=pages, current_page=current_page, genres=genres, current_genre=genre,
            ratings=ratings, current_rating=current_rating)
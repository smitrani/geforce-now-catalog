import json
import os, pymongo, requests
from urllib import response
from dotenv import load_dotenv
from utils.duckduckgo import search
from utils.steam import rating

load_dotenv()

remote_file = 'https://static.nvidiagrid.net/supported-public-game-list/locales/gfnpc-en-US.json'
client = pymongo.MongoClient(os.getenv('MONGO_URI'))
db = client.games

response = requests.get(remote_file)

data = json.loads(response.text)

collection = db.games
collection.delete_many({})
collection.insert_many(data)

processed = 0

for game in collection.find():
    id = game.get('_id')

    if game.get('store') == 'Steam':
        steam_url = game.get('steamUrl')
        steam_appid = steam_url.split('/')[-1]
        game_rating = rating(steam_appid)

        collection.update_one({'_id': id}, {'$set': {
            'image': "https://cdn.cloudflare.steamstatic.com/steam/apps/%s/header.jpg" % steam_appid,
            'rating': game_rating['query_summary']['review_score_desc']
        }})
    else:
        title = game.get('title')
        results = search(f"{title} game cover")

        try:
            image = results['results'][0]['image']
            cloudinary = f'https://res.cloudinary.com/dzzy5gq9e/image/fetch/c_fill,h_215,w_460/{image}'

            collection.update_one({'_id': id}, {'$set': {'image': cloudinary}})
        except:
            print("Error processing record")

    processed += 1
    print("%d games processed\t%s" % (processed, game['title']))